FROM python:3.7

RUN pip install --upgrade pip && \
    pip freeze >requirements.txt && \
    pip install -r requirements.txt

# Expose port 80
EXPOSE 80

# Run app at container launch
CMD ["python", "app.py"]
